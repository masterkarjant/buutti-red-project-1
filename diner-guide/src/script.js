// eslint-disable no-unused-vars
let restaurantData, tagData
let language = 'fi'

document.getElementById("languageSelect").onchange = function() {
    language = document.getElementById('languageSelect').value
    if(document.getElementById('search-input').value) {
        render(restaurantData.filter(r => r.name[language].includes(document.getElementById('search-input').value)))
    } else {
        render(restaurantData)
    }
}
  
async function fetchRestaurantDataAndRender() {
    // apiFetch() from apiFetch.js 
    // eslint-disable-next-line no-undef
    const data = await apiFetch()
    restaurantData = data.restaurants
    tagData = data.tags
    render(restaurantData)
}

fetchRestaurantDataAndRender()

const render = async (data) => {

    console.log(restaurantData)

    document.getElementById('restaurant-list').replaceChildren()

    data.map(restaurant => {

        // SEARCH RESULTS
        const restaurantNameDivOnTheList = document.createElement('div')
        restaurantNameDivOnTheList.innerHTML = restaurant.name[language]

        restaurantNameDivOnTheList.classList.add('clickable')
        restaurantNameDivOnTheList.onclick = () => {

            // RENDER RESTAURANT DETAILS ON CLICK

            // container
            const resultsDisplayContainerDiv = document.createElement('div')
            resultsDisplayContainerDiv.id = 'restaurant-info'

            // header
            const header = document.createElement('h2')
            header.innerText = restaurant.name[language]

            // maps logo
            const a = document.createElement('a')
            a.href = 'https://www.google.fi/maps/place/' + restaurant.location.address.street_address.trim().replace(/\s/g, '+')

            const imgElement = document.createElement('img')
            imgElement.src = './img/google-maps-logo.png'
            imgElement.width = 20

            a.appendChild(imgElement)

            // address
            const addressContainerDiv = document.createElement('div')
            addressContainerDiv.innerText = restaurant.location.address.street_address

            // description
            const divContainingGoogleMapsLinkAndAddress = document.createElement('div')
            divContainingGoogleMapsLinkAndAddress.replaceChildren(a, addressContainerDiv)

            const p = document.createElement('p')
            p.innerText = restaurant.description.body

            resultsDisplayContainerDiv.replaceChildren(header, divContainingGoogleMapsLinkAndAddress, p)
            document.getElementById('restaurant-info').replaceWith(resultsDisplayContainerDiv)

            // toggle fav
            const favButtonDiv = createToggleFavoriteButton(restaurant)
            resultsDisplayContainerDiv.appendChild(favButtonDiv)

            // rating
            const ratingDiv = ceateRatingInterface(restaurant)
            resultsDisplayContainerDiv.appendChild(ratingDiv)
            if (localStorage.getItem(restaurant.id + '-rating'))
                applyRating(localStorage.getItem(restaurant.id + '-rating'), restaurant.id)

            // link to website
            const linkToWebsite = document.createElement('a')
            linkToWebsite.innerText = restaurant.info_url
            linkToWebsite.href = restaurant.info_url
            resultsDisplayContainerDiv.appendChild(linkToWebsite)

            // open hours
            const openingHoursDiv = renderOpenHours(restaurant)
            resultsDisplayContainerDiv.appendChild(openingHoursDiv)

            // display target tags
            const tagsContainerDiv = document.createElement('div')
            tagsContainerDiv.className = 'tagsContainerDiv'
            tagsContainerDiv.innerHTML = '<span style="font-weight: bold; margin-right: 10px;">Tags</span>'
            restaurant.tags.forEach(tag => {
                const tagSpan = document.createElement('span')
                tagSpan.innerHTML = tag.name
                tagSpan.className = 'descriptionTag'
                tagsContainerDiv.appendChild(tagSpan)
            })
            resultsDisplayContainerDiv.appendChild(tagsContainerDiv)

            // comment section
            const commentSection = document.createElement('section')
            commentSection.id = 'commentSection'
            commentSection.innerHTML = `
            <h3 style="font-size: 28px; margin: 5px;">Commets</h3>
            <div>Name<br><input id="commentName" type="text"/></div>
            <div>Comment<br><textarea name="" id="commentText" cols="30" rows="4"></textarea></div>
            <div class="submitCommentBtn" onclick="addNewComment(event, ${restaurant.id})">Comment<div>
            `

            const commentsList = renderComments(restaurant.id)
            commentsList.id = 'commentsList'
            commentSection.appendChild(commentsList)
            resultsDisplayContainerDiv.appendChild(commentSection)

        }

        document.getElementById('restaurant-list').appendChild(restaurantNameDivOnTheList)
    })
}

//-////
// FAV

function createToggleFavoriteButton(targetRestaurant) {

    let alreadyFav = localStorage.getItem(targetRestaurant.id + '-favorite') ? true : false

    const favButtonDiv = document.createElement('div')

    if (alreadyFav) {
        favButtonDiv.className = 'favButton favButtonActive'
        favButtonDiv.innerHTML = '<span>Remove from Favorite</span>'
    }
    else {
        favButtonDiv.innerHTML = '<span>Add to Favorite</span>'
        favButtonDiv.className = 'favButton'
    }

    favButtonDiv.onclick = () => {
        alreadyFav = localStorage.getItem(targetRestaurant.id + '-favorite') ? true : false

        if (alreadyFav) {
            favButtonDiv.innerHTML = '<span>Add to Favorite</span>'
            localStorage.removeItem(targetRestaurant.id + '-favorite')
            favButtonDiv.className = 'favButton'
        }
        else {
            favButtonDiv.innerHTML = '<span>Remove from Favorite</span>'
            localStorage.setItem(targetRestaurant.id + '-favorite', 'true')
            favButtonDiv.className = 'favButton favButtonActive'
        }
    }
    return favButtonDiv
}

function ceateRatingInterface(targetRestaurant) {
    const ratingsContainerDiv = document.createElement('div')
    ratingsContainerDiv.className = 'ratingsContainerDiv'

    ratingsContainerDiv.innerHTML = '<span style="padding: 20px 20px 20px 0">Rating</span>'

    for (let index = 1; index <= 5; index++) {
        const ratingFigureCheckbox = document.createElement('figure')
        ratingFigureCheckbox.innerHTML = index
        ratingFigureCheckbox.style.textAlign = 'center'

        ratingFigureCheckbox.className = 'ratingCheckbox'
        ratingFigureCheckbox.id = 'ratingCheckbox-' + index

        ratingFigureCheckbox.onclick = (event) => {
            applyRating(event.target.id[event.target.id.length - 1], targetRestaurant.id)
        }

        ratingsContainerDiv.appendChild(ratingFigureCheckbox)
    }

    return ratingsContainerDiv
}

//-////
// RATING
function applyRating(rating, targetRestaurantId) {
    const idPrefix = 'ratingCheckbox-'

    for (let index = 1; index <= 5; index++) {
        // first we clear rating
        const checkbox = document.getElementById(idPrefix + index)
        checkbox.className = 'ratingCheckbox'
    }

    for (let index = 1; index <= rating; index++) {
        // then we apply rating
        const checkbox = document.getElementById(idPrefix + index)
        checkbox.className = 'ratingCheckbox ratingCheckboxActive'
    }

    localStorage.setItem(targetRestaurantId + '-rating', rating)
}

//-////
// OPEN HOURS
function renderOpenHours(restaurant) {
    const openingHoursDiv = document.createElement('div')
    openingHoursDiv.style.margin = '15px'
    // openingHoursDiv.style.border = '1px solid black'
    openingHoursDiv.style.marginLeft = '420px'
    openingHoursDiv.style.width = 'fit-content'
    openingHoursDiv.innerHTML = '<span style="font-weight: bold; font-size: 16px">Open Hours</span>'

    restaurant.opening_hours.hours.forEach((dayHours, index) => {
        let day = ''
        if(language === 'fi') {
            if (index == 0)
                day = 'maanantai'
            else if (index == 1)
                day = 'tiistai'
            else if (index == 2)
                day = 'keskiviikko'
            else if (index == 3)
                day = 'torstai'
            else if (index == 4)
                day = 'perjantai'
            else if (index == 5)
                day = 'lauantai'
            else if (index == 6)
                day = 'sunnuntai'
        } else if(language === 'sv') {
            if (index == 0)
                day = 'måndag'
            else if (index == 1)
                day = 'tisdag'
            else if (index == 2)
                day = 'onsdag'
            else if (index == 3)
                day = 'torsdag'
            else if (index == 4)
                day = 'fredag'
            else if (index == 5)
                day = 'lördag'
            else if (index == 6)
                day = 'söndag'
        } else {
            if (index == 0)
                day = 'monday'
            else if (index == 1)
                day = 'tuesday'
            else if (index == 2)
                day = 'wednesday'
            else if (index == 3)
                day = 'thursday'
            else if (index == 4)
                day = 'friday'
            else if (index == 5)
                day = 'saturday'
            else if (index == 6)
                day = 'sunday'
        }

        const weekdayP = document.createElement('p')
        weekdayP.innerHTML = day
        weekdayP.style.margin = '5px'

        const weekdayHoursSpan = document.createElement('span')
        if (dayHours.opens)
            weekdayHoursSpan.innerHTML = ': ' + dayHours.opens.slice(0, 5) + ' - ' + dayHours.closes.slice(0, 5)
        else
            weekdayHoursSpan.innerHTML = ': Closed'
        weekdayP.appendChild(weekdayHoursSpan)
        openingHoursDiv.appendChild(weekdayP)
    })

    return openingHoursDiv
}

//-////
// COMMENT SECTION

// eslint-disable-next-line no-unused-vars
function addNewComment(event, targetRestaurantId) {
    console.log(event.target)
    const commentName = document.getElementById('commentName').value
    const commentText = document.getElementById('commentText').value
    console.log(commentName, commentText, targetRestaurantId)

    const commentIdArr = []
    // We get the length so we dont overwrite the comments with the same id
    console.log(Object.keys(localStorage))
    Object.keys(localStorage).forEach((x, i) => {
        if (x.includes(targetRestaurantId + '-comment-id'))
            commentIdArr.push(i + 1)
    })

    const time = new Date()
    const timeString = time.toDateString() + ' at ' + time.getHours() + ':' + time.getMinutes()

    console.log('commentIdArr', commentIdArr.length)
    const commentInJson = { name: commentName, text: commentText, time: timeString, id: commentIdArr.length }

    localStorage.setItem(targetRestaurantId + '-comment-id-' + commentIdArr.length, JSON.stringify(commentInJson))


    const commentsList = renderComments(targetRestaurantId)
    const previousList = document.getElementById('commentsList')
    commentsList.id = 'commentsList'
    document.getElementById('commentSection').replaceChild(commentsList, previousList)
}

function renderComments(targetRestaurantId) {
    const commentsList = document.createElement('ul')

    Object.entries(localStorage).forEach((commentData) => {
        if (commentData[0].includes(targetRestaurantId + '-comment-id')) {
            const name = JSON.parse(commentData[1]).name
            const text = JSON.parse(commentData[1]).text
            const time = JSON.parse(commentData[1]).time || ''
            const comment = document.createElement('div')
            comment.className = 'commentDiv'
            comment.innerHTML = `
            <span class="commentName">${name}</span>:
            <span class="commentText">${text}</span>
            <br>
            <small>${time}</small>
            `
            commentsList.appendChild(comment)
        }
    })

    return commentsList
}

//-////
// SEARCH RENDER
document.getElementById('search-input').oninput = () => render(restaurantData.filter(r => r.name.fi.startsWith(document.getElementById('search-input').value)))

/* eslint-disable no-unused-vars */


