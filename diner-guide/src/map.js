let map
let mapData
let infoWindow
let markersArray = []

async function fetchMapData() {
    const response = await fetch('https://jsc-myhelsinki-api.azurewebsites.net/v1/places/?tags_filter=Restaurant')
    let mapJSON = await response.json()
    mapData = mapJSON['data']
    initMap()
}

function initMap() {
    // eslint-disable-next-line no-undef
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 60.170, lng: 24.943 },
        zoom: 12,
    })
    // eslint-disable-next-line no-undef
    infoWindow = new google.maps.InfoWindow()
    mapData.forEach((restaurant) => {
        let restaurantLatLong = { lat: restaurant.location.lat, lng: restaurant.location.lon }
        let restaurantContent = '<a href=\'' + restaurant.info_url + '\'><h3>' + restaurant.name.fi + '</h3></a><address>' + restaurant.location.address.street_address +  '</address><p>' + restaurant.description.body + '</p>'
        //let restaurantTags = restaurant["tags"]
        //restaurantTags.forEach((restaurantTag) => {
        //    restaurantContent = restaurantContent + '<button id=\'tagbutton_' + restaurant.id + '_' + tag + '\' data-tag=\'' + tag + '\'>\'' + tag + '</button>'
        //})
        // eslint-disable-next-line no-undef
        const marker = new google.maps.Marker({
            position: restaurantLatLong,
            map,
            title: restaurant.name.fi,
            content: restaurantContent,
            address: restaurant.location.address.street_address,
        })
        // eslint-disable-next-line no-unused-vars
        marker.addListener('click', ({ domEvent, restaurantLatLong }) => {
            // eslint-disable-next-line no-unused-vars
            const { target } = domEvent
            infoWindow.close()
            infoWindow.setContent(marker.content)
            infoWindow.open(marker.map, marker)
        })    
        markersArray.push(marker)                
    })
}
  
// eslint-disable-next-line no-unused-vars
window.addEventListener('load', (event) => {
    fetchMapData()
    document.getElementById('searchButton').onclick = function(event) {
        event.preventDefault()
        removeAllRestaurants()
        let searchString = document.getElementById('searchInput').value
        restaurantSearch(searchString)
    }
})

function removeAllRestaurants() {
    infoWindow.close()
    for (var i = 0; i < markersArray.length; i++ ) {
        markersArray[i].setMap(null)
    }    
}

function restaurantSearch(searchString) {
    markersArray.forEach((marker) => {
        if(marker.title.toLowerCase().includes(searchString.toLowerCase()) || marker.content.toLowerCase().includes(searchString.toLowerCase()) || marker.address.toLowerCase().includes(searchString.toLowerCase())) {
            marker.setMap(map)
        }
    })
}