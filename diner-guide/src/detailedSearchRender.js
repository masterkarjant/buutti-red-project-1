/* eslint-disable no-undef */

//Function to submit the detailed search form
function handleDetailedSearch(event, selectedTags) {
    event.preventDefault()
    const form = document.querySelector('#detailed-search-form')
    const searchResult = detailedSearch(
        restaurantData,
        form.elements['input-name'].value,
        Object.values(selectedTags),
        form.elements['input-location'].value
    )
    //call render
    render(searchResult)
}

//Function to handle clicking tag values. Shows selected above the taglist
function handleTagListClick(selectedTags) {
    const value =  document.querySelector('#input-tag-selection').value
    if (selectedTags[value]) {
        return
    }
    selectedTags[value] = value
    const tags = document.querySelector('#input-selected-tags-div')
    const tagDiv = document.createElement('div')
    const tagId = value.replace(/[^a-z0-9]/gi, '') //remove whitespace and non alphanumeric
    tagDiv.id = tagId
    tagDiv.style.display = 'flex'
    tagDiv.style.gap = '5px'
    tagDiv.style.alignItems = 'center'
    tagDiv.innerText = value

    const removeTagButton = document.createElement('button')
    removeTagButton.innerText = 'X'
    removeTagButton.type = 'button'
    removeTagButton.style.marginLeft = 'auto'
    removeTagButton.onclick = () => {
        delete selectedTags[value]
        document.querySelector(`#${tagId}`).remove()
    }
    tagDiv.appendChild(removeTagButton)
    tags.appendChild(tagDiv)
}

//Generates the template markup for detailed search form
const generateFormMarkup = `
  <form id="detailed-search-form">
      <div>
          <label for="input-name">Name</label>
          <input id="input-name">
      </div>
      <div id="input-selected-tags-div"></div>
      <div>
          <label for="input-tags">Tags</label>
          <select
              id="input-tag-selection"
              name="input-tag-selection"
          >
          </select>
      </div>
      <div>
          <label for="input-location">Location</label>
          <input id="input-location">
      </div>
      <button id="detailed-form-submit" type="submit">Search</button>
  </form>
`

//Shows or hides the detailed search form
// eslint-disable-next-line no-unused-vars
function renderDetailedSearch() {
    //Selected Tags is an object to which selected tags are added to
    const selectedTags = {}
    const detailedSearchDiv = document.querySelector('#detailed-search-div')
    const searchButton = document.querySelector('#detailed-search-button')
    const normalSearchInput = document.querySelector('#search-input')

    //close detailed search if it's open and show normal search instead
    if (detailedSearchDiv.childNodes.length) {
        normalSearchInput.style.display = 'block'
        searchButton.innerText = 'Detailed search'
        detailedSearchDiv.replaceChildren()
        render(restaurantData)
        return
    }
    //else generate form template markup
    detailedSearchDiv.insertAdjacentHTML('beforeend', generateFormMarkup)

    /*
      add onchange event to tag selection and add tag options to form select
      tagData is fetched from script.js
  */
    const tagSelect = document.querySelector('#input-tag-selection')
    tagSelect.onchange = () => handleTagListClick(selectedTags)

    Object.values(tagData).forEach(tag => {
        tagSelect.insertAdjacentHTML(
            'beforeend',
            `<option value="${tag}">${tag}</option>`
        )
    })

    //add onclick to submitbutton
    const submitButton = document.querySelector('#detailed-form-submit')
    submitButton.onclick = event => handleDetailedSearch(event, selectedTags)

    //remove default search input and change searchbutton innerText
    normalSearchInput.style.display = 'none'
    searchButton.innerText = 'Close detailed search'
}