/* eslint-disable no-undef */
//Standard search
document.getElementById('search-input').oninput = () => {
    language = document.getElementById('languageSelect').value
    render(restaurantData.filter(r => {
        if(r.name[language] !== null) {
            //console.log(r.name[language])
            r.name[language].includes(document.getElementById('search-input').value)
        }
    }))
}
//detailed search
// eslint-disable-next-line no-unused-vars
function detailedSearch(restaurants, searchStr, tags, address, exclusive = true) {
    language = document.getElementById('languageSelect').value
    let data = restaurants.filter(r => {
        if(r.name[language] !== null) {
            const lowerName = r.name[language].toLowerCase()
            const lowerSearch = searchStr.toLowerCase()
            return lowerName.includes(lowerSearch)
        } else {
            return false
        }
    })

    if(tags !== undefined){
        if(exclusive){
            data = data.filter(r => {
                const hasAnyTag = tags.every((inputTag) => {
                    return r.tags.some(restaurantTag => {
                        const restaurantTagLower = restaurantTag.name.toLowerCase()
                        const inputTagLower = inputTag.toLowerCase()
                        return restaurantTagLower === inputTagLower
                    })
                })
                return hasAnyTag
            })
        }else{
            data = data.filter(r => {
                const hasAnyTag = r.tags.some((restaurantTag) => {
                    const rTagInUTag = tags.some(userTag => {
                        const restaurantTagLower = restaurantTag.name.toLowerCase()
                        const userTagLower = userTag.toLowerCase()
                        return restaurantTagLower === userTagLower
                    })
                    return rTagInUTag
                })
                return hasAnyTag
            })
        }
    }

    if(address !== undefined){
        data = data.filter(r => {
            const lowerStreetAddress = r.location.address.street_address.toLowerCase()
            const lowerUserAddress = address.toLowerCase()
            return lowerStreetAddress.startsWith(lowerUserAddress)
        })
    }

    return data
}