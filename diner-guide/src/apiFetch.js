
const BASE_URL = 'https://jsc-myhelsinki-api.azurewebsites.net/v1/places/?tags_filter=Restaurant'

// eslint-disable-next-line no-unused-vars
async function apiFetch() {
    try {
        const response = await fetch(BASE_URL)
        const jsonData = await response.json()
        return { 
            restaurants: jsonData.data,
            tags: jsonData.tags
        }
    }
    catch (error) {
        console.log(error)
    }
}

//module.exports = { apiFetch }