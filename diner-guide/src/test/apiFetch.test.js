
const { apiFetch } = require('../apiFetch')

describe('test data fetch', () => {
    it('fetches data', async () => {
        const res = await apiFetch()
        expect(res.length).toBeTruthy()
    })
})