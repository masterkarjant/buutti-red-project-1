# Project Backlog



GET ALL REQUIRED API DATA
Display all data 
Have detailed search feature
Complete design and layout

Definition of Done : Lint ei saa valittaa, on tehty testit ja ne menee läpi

DAY 1 
Setup work environment - Git / Branch

GET ALL REQUIRED API DATA - 4h {
1. Use MyHelsinki API instead of hard-coded restaurant list - 

1. I want to see the restaurant opening hours
1. I want to see a link to the restaurant web site
1. I want to see the restaurant tags
}

FIX STUFF - {
2. Remove unused dependencies - 1min
2. Fix linter problems 
2. Some tests fail -> fix this  - KYSY JARILTA
}

DAY 2



SEARCH - Antti - Taneli {
3. Bugfix: Search finds only perfect matches - 2.6h - {
    We want to work with partial matches
    partial = Kunhan ravintola sisältää hakusanan
    3. Add tests to cover the search function
}

1. Epic: Detailed Search page
    - search by name
    - filter by tags
    - search by location (address or coordinates)

    - create UI for opening detailed search box
    - Create search box UI
    - Filter data by tags
    - Filter data by locations

    3. I want to be able to click a tag to see all matching restaurants
}

DESIGN AND LAYOUT - 90min Taneli {
S. Use centered layout with margins on both sides
S. Apply new colour & fonts scheme
}

LOCAL STORAGE - Anton - Tomi - {
1. I want to be able to save a list of my favorite restaurants
1. I want to be able to remove restaurants from my favorites

1. I want to be able to filter restaurants according to my ratings
1. I want to be able to add ratings to restaurants

1. I want to be able to add comments to restaurants
}

DAY 3

TARKISTA TILANNE StandUpin aikana
1. Documentation - Intentio commentoida ja käyttää selkeitä funktio nimiä
Voidaan antaa käyttäjälle jotta hän osaa käyttää tuotetta ilman ongelmia
Jotta voidaan siirtää projekti toiselle tiimille niin että he ymmärtäisivät koodia paremmin
1. Refactor the script.js to readable code


1. Epic: Map page - Tomi
    - ability to see selected restaurants in the map
    - ability to search for restaurants in the selected area



1. Epic: Multi-lingual support - Anton - Taneli
    - fi/sv/en
    - Localized restaurant information
    - App text
    - Default from browser, user-selection



